using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace CustomArray
{
    public class CustomArray<T> : IEnumerable<T>
    {
        private T[] array;
       
        private int first, last, length;
        /// <summary>
        /// Should return first index of array
        /// </summary>

        public int First
        
        {
            get { return first; }
            private set
            {
                first = value;
                last = first + Length - 1;
            }
        }
        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last
        
        {
            get { return last; }
        }
        /// <summary>
        /// Should return length of array
        /// <exception cref="ArgumentException">Thrown when value was smaller than 0</exception>
        /// </summary>

        public int Length
        
        {
            get { return length; }
            private set
            {
                if (value > 0.0)
                {
                    length = value;
                }
                else
                {
                    throw new ArgumentException("Value of length is smaller than 0 ");
                    
                }

            }
        }
        /// <summary>
        /// Should return array by its reference 
        /// </summary>       

        public T[] Array
       
        {
            get
            {
                return array;
               
            }

        }
        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>        
        public CustomArray(int first, int length)
        
        {
            Length = length;
            First = first;
            array = new T[Length];

        }
        /// <summary>
        /// Constructor with first index and collection  
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        ///  <exception cref="NullReferenceException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when count is smaler than 0</exception>
        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list != null)
            {
                if (list.Count() > 0)
                {
                    Length = list.Count();
                    First = first;
                    
                    array = new T[Length];
                    
                    
                    array = list.ToArray();
                    
                }
                else
                {
                    throw new ArgumentException("List contains 0 elements");
                }
            }
            else
            {
                throw new NullReferenceException("List is null");
            }
        }



        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when list without elements </exception>
        public CustomArray(int first, params T[] list)
        {
            if (list != null)
            {
                if (list.Length > 0)
                {
                    Length = list.Length;
                    First = first;
                    array = new T[Length];
                    
                    array = list;
                    
                }
                else
                {
                    throw new ArgumentException("list has no elements ");
                    
                }
            }
            else
            {
                throw new ArgumentNullException("list is null");
            }
        }



        /// <summary>
        /// Indexer with get and set  properties
        /// </summary>
        /// <param name="item">Int index</param>        
        /// <returns> Return element of array at specific index</returns>
        /// <exception cref="ArgumentException">Thrown when index out of array range</exception>       
        public T this[int item]
        {
            get
            {
                var edge = item - First;
                
                if (edge >= 0 && edge <= Length - 1)
                {
                    return array[edge];
                }
                throw new ArgumentException();
               
            }
            set
            {
                if (value != null)
                {
                    var edge = item - First;

                    if (edge >= 0 && edge <= Length - 1)
                    {
                        array[edge] = value;
                    }
                    else
                    {
                        throw new ArgumentException();
                    }
                }
                else
                {
                    throw new ArgumentNullException();
                }

            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < array.Length; i++)
            {
                yield return array[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
            
        }
    }
}

